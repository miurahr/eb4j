# EB4j

[![Build status](https://dev.azure.com/miurahr/CodeBerg/_apis/build/status/CodeBerg-eb4j-CI)](https://dev.azure.com/miurahr/CodeBerg/_build/latest?definitionId=23)

EPWING/Ebook access library.

## Use EB4J library for your project

### Java Platform Module System

Please use "tokyo.northside.eb4j" for module name.

### Gradle configuration

```
dependencies {
    implementation 'tokyo.northside:eb4j:4.1.0'
}
```

### Gradle(kts)

```
dependencies {
    implementation("tokyo.northside:eb4j:4.1.0")
}
```

## Maven configuration

```
 <dependencies>
    <dependency>
      <groupId>tokyo.northside</groupId>
      <artifactId>eb4j</artifactId>
      <version>4.1.0</version>
      <type>jar</type>
    </dependency>
    ...
  </dependencies>
```


## Build

EB4j uses Gradle for a build system. You can build a library and utilities
by typing command (in Mac/Linux/Unix):

```
$ ./gradlew build
```

or (in Windows):

```
C:> gradlew.bat build
```

## Contribution

As usual of other projects hosted on GitHub, we are welcome a forking source and send modification as a Pull Request.
It is recommended to post an issue before sending a patch, and share your opinions and/or problems.

## Copyrights and License

EB4j, an access library for EPWING/Ebook.

Copyright(C) 2003-2010 Hisaya FUKUMOTO

Copyright(C) 2016 Hiroshi Miura, Aaron Madlon-Kay

Copyright(C) 2020-2023 Hiroshi Miura

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

## Why forked and welcome contribution

Here is a fork project from http://eb4j.osdn.jp which discontinued its development in 2010.

Here is a new place for an eb4j project in order to maintain it by community
basis, to accept patches and comments, and to improve the library.

You are welcome to contribute here by pushing PRs, leaving comments or
join as development member.
