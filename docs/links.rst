Technical references 技術資料
----------------------------

* 12cmＣＤ(EPWING)と、電子ブック(EB)のデータ構造について `cdrom spec`_
* `JIS X 4081`_
* `EBライブラリ`_
* FreePWING_
* `EBstudio Unicodeマッピング`_

Projects who use EB4J 2.0.0 and later
--------------------------------------

* OmegaT CAT tool plugin for EPWING dictionary `omegat-epwing`_

.. Links
.. _`JIS X 4081`: https://www.jisc.go.jp/app/jis/general/GnrJISNumberNameSearchList?show&jisStdNo=X4081
.. _`cdrom spec`: http://www.nerimadors.or.jp/~jiro/cdrom2/doc/spec
.. _`EBライブラリ`: https://github.com/mistydemeo/eb
.. _FreePWING: http://openlab.ring.gr.jp/edict/fpw/
.. _`EBstudio Unicodeマッピング`: https://ebstudio.hatenablog.com/entry/20171018/p1
.. _`omegat-epwing`: https://codeberg.org/miurahr/omegat-plugin-epwing
