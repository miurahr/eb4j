About eb4j
==========

eb4j is a 100% pure Java library to access electric dictionaries and eBooks based on `EB library`_.

.. _`EB library`: http://www.sra.co.jp/people/m-kasahr/eb/index.html

Original author is Hisaya FUKUMOTO and original eb4j is distributed on eb4j.sourceforge.jp
The original development and versions had stopped in 2010.

Here is a fork and active development version.

How to install the library
--------------------------

You can install the library from bintray maven repository.

https://bintray.com/beta/#/eb4j/maven/eb4j-core?tab=overview

You need to add a above repository to your project.

Here is an example for configurations;

For Gradle;

.. code-block::

    implementation 'io.github.eb4j:eb4j:2.0.0'

To use with Maven:

.. code-block::

    <dependency>
        <groupId>tokyo.northside</groupId>
        <artifactId>eb4j</artifactId>
        <version>2.0.0</version>
        <type>pom</type>
    </dependency>


Contacts
--------

You are welcome to make questions, join developments and add documents on wiki!

- Project home: https://codeberg.org/miurahr/eb4j

- Issues: https://codeberg.org/miurahr/eb4j/issues

- Wiki: https://codeberg.org/miurahr/eb4j/wiki

Copyright
=========

EB4J - EPWING/Ebook access library and utilities.

Copyright(C) 2020 Hiroshi Miura

Copyright(C) 2016 Hiroshi Miura, Aaron Madlon-Kay

Copyright(C) 2003-2010 Hisaya FUKUMOTO


This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
