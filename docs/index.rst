.. eb4j documentation master file, created by
   sphinx-quickstart on Sat Jul 16 11:01:22 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to eb4j's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   usage
   links

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
