package tokyo.northside.eb4j.ext;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Test case for UnicodeEscaper class.
 */
public class UnicodeEscaperTest {

    private UnicodeEscaper unicodeEscaper;

    @Test(groups = "init")
    public void testConstructor() {
        unicodeEscaper = new UnicodeEscaper();
        assertNotNull(unicodeEscaper);
    }

    /**
     * Test translating unicode to escape sequence.
     */
    @Test
    public void testTranslate() {
        assertEquals(unicodeEscaper.translate("\u00e1"), "\\u00E1");
        assertEquals(unicodeEscaper.translate("\u00E6\u0305"), "\\u00E6\\u0305");
        assertEquals(unicodeEscaper.translate(String.valueOf(Character.toChars(0x00e1))), "\\u00E1");
        assertEquals(unicodeEscaper.translate(String.valueOf(Character.toChars(0x0001F309))), "\\U0001F309");
    }

}
