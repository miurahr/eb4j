package tokyo.northside.eb4j.ext;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Test case for UnicodeUnescaper class.
 */
public class UnicodeUnescaperTest {

    private UnicodeUnescaper unicodeUnescaper;

    @Test(groups = "init")
    public void testConstructor() {
        unicodeUnescaper = new UnicodeUnescaper();
        assertNotNull(unicodeUnescaper);
    }

    /**
     * Test to convert escape sequence to unicode.
     */
    @Test
    public void testTranslate() {
        assertEquals(unicodeUnescaper.translate("\\u00e1"), "\u00E1");
        assertEquals(unicodeUnescaper.translate("\\u00e6\\u0305"), "\u00E6\u0305");
        assertEquals(unicodeUnescaper.translate("\\u00e1"), String.valueOf(Character.toChars(0x00e1)));
        assertEquals(unicodeUnescaper.translate("\\U0001F309"), String.valueOf(Character.toChars(0x0001F309)));
    }
}
