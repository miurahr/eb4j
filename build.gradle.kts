import java.io.FileInputStream
import java.util.Properties

plugins {
    checkstyle
    jacoco
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.nexus.publish)
    alias(libs.plugins.git.version) apply false
    alias(libs.plugins.sphinx)
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.commons.lang3)
    implementation(libs.commons.text)
    testImplementation(libs.testng)
    testRuntimeOnly(libs.slf4j.simple)
}

tasks.getByName<Test>("test") {
    useTestNG()
}

jacoco {
    toolVersion=libs.versions.jacoco.get()
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        html.required.set(true)
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

tasks.jar {
    manifest {
        attributes("Automatic-Module-Name" to "tokyo.northside.eb4j")
    }
}

checkstyle {
    config = resources.text.fromFile("${rootProject.projectDir}/config/checkstyle/checkstyle.xml")
    toolVersion = libs.versions.checkstyle.get()
}

spotbugs {
    excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

// we handle cases without .git directory
val versionProperties = project.file("src/main/resources/tokyo/northside/eb4j/version.properties")
val dotgit = project.file(".git")

if (dotgit.exists()) {
    apply(plugin = "com.palantir.git-version")
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("EB4J")
                    description.set("EPWING/Ebook access library")
                    url.set("https://codeberg.org/miurahr/eb4j")
                    licenses {
                        license {
                            name.set("The GNU Lesser General Public License, Version 2.1")
                            url.set("https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html")
                            distribution.set("repo")
                        }
                    }
                    developers {
                        developer {
                            id.set("miurahr")
                            name.set("Hiroshi Miura")
                            email.set("miurahr@linux.com")
                        }
                    }
                    scm {
                        connection.set("scm:git:git://codeberg.org/miurahr/eb4j.git")
                        developerConnection.set("scm:git:git://codeberg.org/miurahr/eb4j.git")
                        url.set("https://codeberg.org/miurahr/eb4j")
                    }
                }
            }
        }
    }

    val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

    signing {
        when (signKey) {
            "signingKey" -> {
                val signingKey: String? by project
                val signingPassword: String? by project
                useInMemoryPgpKeys(signingKey, signingPassword)
            }
            "signing.keyId" -> { /* do nothing */ }
            "signing.gnupg.keyName" -> {
                useGpgCmd()
            }
        }
        sign(publishing.publications["mavenJava"])
    }

    nexusPublishing.repositories {
        sonatype()
    }
} else if (versionProperties.exists()) {
    version = Properties().apply { load(FileInputStream(versionProperties)) }.getProperty("version")
}

tasks.register("writeVersionFile") {
    val folder = versionProperties.parentFile
    if (!folder.exists()) {
        folder.mkdirs()
    }
    versionProperties.delete()
    versionProperties.appendText("version=" + project.version)
}

tasks.getByName("jar") {
    dependsOn("writeVersionFile")
}

tasks.sphinx {
    sourceDirectory {"docs"}
}
