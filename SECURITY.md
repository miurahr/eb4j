# Security Policy

## Supported Versions

Only recent version of eb4j is currently supported with security updates.

| Version | Supported          |
| ------- |--------------------|
| 4.1.x   | :heavy_check_mark: |
| 4.0.x   | :x:                |
| 3.0.x   | security fix only  |
| < 3.0   | :x:                |

## Reporting a Vulnerability

Please disclose security vulnerability privately at miurahr@linux.com
